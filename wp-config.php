<?php
error_reporting(E_ALL); ini_set('display_errors', 0);


/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pinisi_landing');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|lnch$.|F>9+WE<-~wY^+$]F5^;nx6i@>t(KH0UcJVjEPx^se(&|<$IS~{La?6{N');
define('SECURE_AUTH_KEY',  ',<{VP;(<g4bTtplTFg3X6pG+IWB$ZG:3 #M(w;#e|;R?q_fA(>GBq+f}(Q;A|o-8');
define('LOGGED_IN_KEY',    '7n<[#?I{uL1ym^3Omg},^$+g0>}%2!6@?9]F<~w`j9#|d 2J[D|ajD )YTr.6y^^');
define('NONCE_KEY',        '!4ab]ioi}8D;)G8 ,v>~?l-:gdlkt3z&u}!LO9!BiKnb5eN:+6ee}.-imB={ W5I');
define('AUTH_SALT',        '<Y1FK3FaO.@m^x=+)wm`B{~#f)Ci{?^Yt,z!6B|iSkLQf~hd9l v+:%?yJ>!{dNY');
define('SECURE_AUTH_SALT', 'E%5A*TS9lMqD3!MjT]--^CQFX&/|pb,[3]k,Y)m%pE56aA7`MgoL=&*^_)Ugh^Y5');
define('LOGGED_IN_SALT',   'b]gS-5w+T$mg| }>I1)Jf~<P}2)*HO]?,d.R|K#r[+XAmNrSJ*K<e:?GhoO+h}iM');
define('NONCE_SALT',       'p4xnY9r{tey-O#9`_JKHgdy AFR`sj+Vy~Wvkv$K|y+{5`uD)E}p<0e!(%RE)=:x');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pnz_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
